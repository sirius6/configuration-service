FROM openjdk:11-jre-slim

# Set time zone
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Start server
COPY build/libs/configuration-service-*.jar /app/configuration-service.jar
WORKDIR "/app"
ENTRYPOINT ["java", "-jar", "/app/configuration-service.jar", "-Xmx64m"]

EXPOSE 8888
