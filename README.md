# Configuration service

This service is used to centralize configuration within services.

All "sirius" service should use this service for configuration.

The configuration service is available on port 8888.


## Use encrypted configuration

It is often useful to use ciphering for sensitive values (such as passwords). For this you have to launch the
service and use a REST API to get a ciphered value.

For this, you have to set the environment variable `ENCRYPT_KEY`, then you have to request a REST API:

```
curl -X POST --data-urlencode "my password" http://sirius:sirius@localhost:8888/encrypt
```