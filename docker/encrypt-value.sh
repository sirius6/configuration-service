#!/bin/bash
set -e
ENC_VALUE=$(curl -s -X POST --data-urlencode "$1" http://sirius:sirius@localhost:8888/encrypt)
echo "{cipher}$ENC_VALUE"